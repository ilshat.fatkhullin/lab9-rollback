import java.sql.*;
import java.util.LinkedList;

public class Main {
    // JDBC driver name and database URL
    static final String DB_URL = "jdbc:postgresql://localhost:5432/company";

    //  Database credentials
    static final String USER = "username";
    static final String PASS = "password";

    public static void main(String[] args) {
        var ids = new LinkedList<Integer>();
        ids.add(1);
        ids.add(2);
        ids.add(3);

        var employeeIds = new LinkedList<Integer>();
        employeeIds.add(106);
        employeeIds.add(107);
        employeeIds.add(108);

        var amount = new LinkedList<Integer>();
        amount.add(100);
        amount.add(200);
        amount.add(300);

        addSalaries(ids, employeeIds, amount);
    }

    public static void addSalaries(LinkedList<Integer> ids, LinkedList<Integer> employeeIds, LinkedList<Integer> amount) {
        Connection connection = null;
        Statement statement = null;

        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try {
            connection = DriverManager.getConnection(DB_URL, USER, PASS);
            connection.setAutoCommit(false);

            statement = connection.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            for (var i = 0; i < Math.max(ids.size(), Math.max(employeeIds.size(), amount.size())); i++) {
                System.out.println("Inserting one row....");
                statement.executeUpdate("INSERT INTO public.salary " +
                        "VALUES (" + ids.get(i) + "," +
                        employeeIds.get(i) + "," +
                        amount.get(i) + ")");
            }

            connection.commit();
            statement.close();
            connection.close();
        } catch (SQLException | IndexOutOfBoundsException se) {
            // Rollback
            se.printStackTrace();
            try {
                if (connection != null)
                    connection.rollback();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (statement != null)
                    statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            try {
                if (connection != null)
                    connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main2(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            System.out.println("Connecting to database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            conn.setAutoCommit(false);


            System.out.println("Creating statement...");
            stmt = conn.createStatement(
                    ResultSet.TYPE_SCROLL_INSENSITIVE,
                    ResultSet.CONCUR_UPDATABLE);

            System.out.println("Inserting one row....");
            var SQL = "INSERT INTO Employees " +
                    "VALUES (108, 20, 'Rita', 'Tez')";


            System.out.println("Inserting one row....");
            SQL = "INSERT INTO Employees " +
                    "VALUES (106, 20, 'Rita', 'Tez')";
            stmt.executeUpdate(SQL);
            SQL = "INSERT INTO Employees " +
                    "VALUES (107, 22, 'Sita', 'Singh')";
            stmt.executeUpdate(SQL);

            // Commit data here.
            System.out.println("Commiting data here....");
            conn.commit();

            String sql = "SELECT id, first, last, age FROM Employees";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println("List result set for reference....");
            printRs(rs);
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
            // If there is an error then rollback the changes.
            System.out.println("Rolling back data here....");
            try {
                if (conn != null)
                    conn.rollback();
            } catch (SQLException se2) {
                se2.printStackTrace();
            }//end try

        } catch (Exception e) {
            //Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            //finally block used to close resources
            try {
                if (stmt != null)
                    stmt.close();
            } catch (SQLException se2) {
            }
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }
        System.out.println("Goodbye!");
    }

    public static void printRs(ResultSet rs) throws SQLException {
        //Ensure we start with first row
        rs.beforeFirst();
        while (rs.next()) {
            //Retrieve by column name
            int id = rs.getInt("id");
            int age = rs.getInt("age");
            String first = rs.getString("first");
            String last = rs.getString("last");

            //Display values
            System.out.print("ID: " + id);
            System.out.print(", Age: " + age);
            System.out.print(", First: " + first);
            System.out.println(", Last: " + last);
        }
        System.out.println();
    }

}
